import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  {
  path: '',
  loadChildren: () => import('./modules/home-and.module').then(m => m.HomeAndModule)
},
{
  path: 'home',
  loadChildren: () => import('./modules/home-and.module').then(m => m.HomeAndModule),
  data: {
    breadcrumbs: 'Inicio'
  }
},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
