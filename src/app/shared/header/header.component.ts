import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  RutaJsonHeader :string = "./assets/JsonData/General.json";
   Dataheader : any;
   Dataloaded : boolean=false;

   constructor(private http: HttpClient)
  {
  }

  ngOnInit()
  {
    this.LeerInformacionHeader();
  }
  
  LeerInformacionHeader()
  {
    this.http.get(this.RutaJsonHeader).toPromise().then(data=> {
      this.Dataheader = data;
      this.Dataloaded = true;
     });
  
 }
}
