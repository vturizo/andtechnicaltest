import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

  RutaJsonHeader :string = "./assets/JsonData/General.json";
   DataBanner : any;
   Dataloaded : boolean=false;

   constructor(private http: HttpClient)
  {
  }

  ngOnInit()
  {
    this.LeerInformacionBanner();
  }
  
  LeerInformacionBanner()
  {
    this.http.get(this.RutaJsonHeader).toPromise().then(data=> {
      this.DataBanner = data;
      this.Dataloaded = true;
     });
  
 }
}
