import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  RutaJsonFooter :string = "./assets/JsonData/General.json";
  DataFooter : any;
  Dataloaded : boolean=false;

  constructor(private http: HttpClient)
 {
 }

 ngOnInit()
 {
   this.LeerInformacionHeader();
 }
 
 LeerInformacionHeader()
 {
   this.http.get(this.RutaJsonFooter).toPromise().then(data=> {
     this.DataFooter = data;
     this.Dataloaded = true;
    });
  }
}
