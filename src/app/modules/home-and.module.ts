import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeAndRoutingModule } from './home-and-routing.module';
import { HomeComponent } from './home/home.component';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeAndRoutingModule
  ]
})
export class HomeAndModule { }
